FogBlog
=======

is a continue of a [project I use](https://gitlab.com/hyperclock/symfony-i18n-base) as a basic i18n [Symfony Framework](https://symfony.com) for various other projects. This is one of them.


## Features
* i18n
  * English (default)
  * German (secondary)
  * ...other translations welcome!
* Cookie info popup
* Prepared to have pages(static) added

## Planned Features
* Blog, i18n ready and usable
* Backend (Admin Area)
* Frontend (User Administration)

## Documentation
Coming soon. Keep a eye on the [wiki](../../wikis)

## License
**BSD 3-Clause "New" or "Revised" License**

A permissive license similar to the BSD 2-Clause License, but with a 3rd clause that prohibits others from using the name of the project or its contributors to promote derived products without written consent.

***Permissions***
* Commercial use
* Modification
* Distribution
* Private use

***Limitations***
* No Liability
* No Warranty

***Conditions***
* License and copyright notice
  * -must be included with the software
